FROM debian:11.6-slim

RUN apt update && apt install -y nftables sudo

COPY nftables-docker-entrypoint.sh /opt/

RUN ls -A1 /etc/skel | xargs rm -rf && useradd -m -s /bin/bash nftables && \
  echo "nftables ALL = (root) NOPASSWD: /usr/sbin/nft" > /etc/sudoers.d/10-nftables && \
  chmod 0440 /etc/sudoers.d/10-nftables && \
  mkdir /srv/nftables && chown nftables: /srv/nftables && \
  chmod +x /opt/nftables-docker-entrypoint.sh

USER nftables

WORKDIR /srv/nftables

ENTRYPOINT ["/opt/nftables-docker-entrypoint.sh"]