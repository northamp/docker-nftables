#!/bin/bash

if [ $# -eq 0 ]
  then
    set -e
    echo "Attempting to load rules set in /srv/nftables/ruleset.nft..."
    sudo nft -f /srv/nftables/ruleset.nft
    trap "Caught stop signal, flushing then leaving; sudo nft flush ruleset; exit" TERM EXIT
    echo "Now monitoring..."
    sudo nft monitor
else
  $@
fi
