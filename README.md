# Dockerized nftables

Extremely simple Docker image with Nftables installed. Able to load rules from a mount point.

Needs `NET_ADMIN` capability to work (TODO: investigate a way to do without).

## Quickstart

```bash
docker run -d --cap-add NET_ADMIN -v ${PWD}/backup.nft:/srv/nftables/backup.nft -p 7777:7777 registry.gitlab.com/northamp/docker-nftables/nftables:latest
```

## Configuration

If no startup argument was provided, the entrypoint loads a nftables backup file (which can be generated with [`nft list ruleset >> backup.nft`](https://wiki.nftables.org/wiki-nftables/index.php/Operations_at_ruleset_level#backup.2Frestore)) from the container's `/srv/nftables/backup.nft` and then runs `nft monitor` to stdout for easy logging.
